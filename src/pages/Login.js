import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext'; 

export default function Login() {

   const {user, setUser} = useContext(UserContext)

   const [email, setEmail] = useState('');
   const [password, setPassword] = useState('');

   const [isActive, setIsActive] = useState(false)

   console.log(email);
   console.log(password);

   useEffect(() => {
      // validation to enable the register button when all fields are populated and both password match.
      if(email !== '' && password !== '') {
         setIsActive(true)
      }
      else {
         setIsActive(false)
      }
   });

   function loginUser(e) {

      e.preventDefault();

      localStorage.setItem('email', email);

      setUser({
         email: localStorage.getItem('email')
      })

      setEmail('');
      setPassword('');

      alert('You are now logged in!')
   }

   return (

      (user.email !== null) ? <Navigate to="/courses"/> :
      <>
         <h1> Login </h1>
         <Form onSubmit={e => loginUser(e)}>
             <Form.Group className="mb-3" controlId="userEmail">
               <Form.Label>Email Address</Form.Label>
                 <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
             </Form.Group>

             <Form.Group className="mb-3" controlId="password1">
                 <Form.Label>Password</Form.Label>
                 <Form.Control type="password" placeholder="Confirm Password" value={password} onChange={e => setPassword(e.target.value)} required/>
             </Form.Group>
             
             {/* Conditional Rendering -> If the button is active, it should be clickable, else it should be unclickable*/}
             {
               isActive ? <Button variant="primary" type="submit" id="submitBtn">Login</Button> : <Button variant="primary" type="submit" id="submitBtn" disabled>Login</Button>
             }
          </Form>
      </>
   )
}