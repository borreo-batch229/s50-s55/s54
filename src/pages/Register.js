import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext'; 


export default function Register() {

   const {user} = useContext(UserContext)

   // State Hooks -> stores value of the input fields.
   const [email, setEmail] = useState('');
   const [password1, setPassword1] = useState('');
   const [password2, setPassword2] = useState('')

   const [isActive, setIsActive] = useState(false)

   console.log(email);
   console.log(password1);
   console.log(password2);

   useEffect(() => {
      // validation to enable the register button when all fields are populated and both password match.
      if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
         setIsActive(true)
      }
      else {
         setIsActive(false)
      }
   });

   function registerUser(e) {

      e.preventDefault();

      setEmail('');
      setPassword1('');
      setPassword2('');

      alert('Thank you for registering!')
   }

   return (

      (user.email !== null) ? <Navigate to="/courses"/> :
      <>
         <h1> Register </h1>
         <Form onSubmit={e => registerUser(e)}>
             <Form.Group className="mb-3" controlId="userEmail">
               <Form.Label>Email address</Form.Label>
                 <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
                 <Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
             </Form.Group>

             <Form.Group className="mb-3" controlId="password1">
                 <Form.Label>Password</Form.Label>
                 <Form.Control type="password" placeholder="Confirm Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
             </Form.Group>

             <Form.Group className="mb-3" controlId="password2">
                 <Form.Label>Confirm Password</Form.Label>
                 <Form.Control type="password" placeholder="Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
             </Form.Group>
             
             {/* Conditional Rendering -> If the button is active, it should be clickable, else it should be unclickable*/}
             {
               isActive ? <Button variant="primary" type="submit" id="submitBtn">Register</Button> : <Button variant="primary" type="submit" id="submitBtn" disabled>Register</Button>
             }
          </Form>
      </>
   )
}